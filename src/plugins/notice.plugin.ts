import { App } from "vue";
import { ElMessage } from "element-plus";

export default {
  install: (app: App) => {
    app.config.globalProperties.$notice = (message: string, type = "info") => {
      switch (type) {
        case "success":
          ElMessage({
            message: message,
            type: "success",
          });
          break;

        case "error":
          ElMessage({
            type: "error",
            message: message,
          });
          break;

        case "warning":
          ElMessage({
            type: "warning",
            message: message,
          });
          break;

        case "info":
          ElMessage({
            type: "info",
            message: message,
          });
          break;

        default:
          break;
      }
    };
  },
};
