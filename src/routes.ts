import { createRouter, createWebHistory } from "vue-router";
import Container from "./layouts/wrappers/Container.vue";

const Blank = () => import("./views/Blank.vue");
const Test = () => import("./views/test.vue");
const Page1 = () => import("./views/pages/page1/Page1.vue");
const Page2 = () => import("./views/pages/page2/Page2.vue");
const Page3 = () => import("./views/pages/page3/Page3.vue");

let children: any[] = [
  // { name: "Blank", path: "/blank", component: Blank },
  { name: "Page1", path: "/page1", component: Page1 },
  { name: "Page2", path: "/page2", component: Page2 },
  { name: "Page3", path: "/page3", component: Page3 },
];

const routes = [
  {
    path: "/",
    name: "Container",
    component: Container,
    redirect: "page1",
    children: children,
  },
];

const router = createRouter({
  history: createWebHistory(),
  linkActiveClass: "active",
  routes,
});

export default router;
