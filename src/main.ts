import "./assets/index.css";

import mn from "element-plus/es/locale/lang/mn";
import "element-plus/dist/index.css";

import { Router } from "vue-router";
import routes from "./routes";
import noticePlugin from "./plugins/notice.plugin";
import { createApp } from "vue";
import App from "./App.vue";

declare module "vue" {
  interface ComponentCustomProperties {
    $router: Router;
    $notice: Function;
  }
}

const app = createApp(App);

app.use(routes);
app.use(noticePlugin);

app.mount("#app");
